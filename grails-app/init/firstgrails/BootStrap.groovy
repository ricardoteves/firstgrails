package firstgrails

class BootStrap {

    def init = { servletContext ->
        // Book
        def stephen = new Author(name: "Stephen King")
        def james = new Author(name: "James Patterson")
        def robert = new Author(name: "Robert Ludlum")
        def a =new Book(isbn: "123456", title:"The Shining", Author: stephen)
        def b =new Book(isbn: "7777777", title:"The Knowledge", Author: stephen)
        def c =new Book(isbn: "765745757", title:"The Knowledge", Author: stephen)
        def d = new Book(isbn: "234909853423", title:"The Cold War", Author: stephen)
        def e = new Book(isbn: "654321", title:"Along Came a Spider", Author: james)
        def f = new Book(isbn: "23425655", title:"The Man of Steel", Author: james)
        def g = new Book(isbn: "6739043067", title:"Spicy Nuts", Author: robert)
        stephen.addToBooks(a)
        stephen.addToBooks(b)
        stephen.addToBooks(c)
        stephen.addToBooks(d)
        james.addToBooks(e)
        james.addToBooks(f)
        robert.addToBooks(g)
        stephen.save()
        james.save()
        robert.save()

        // Car
        new CarModel(maker: "Ford", color: "Black", year: 2017, model: "Fiesta").save()
        new CarModel(maker: "Ford", color: "Black", year: 2018, model: "Fiesta").save()
        new CarModel(maker: "BMW", color: "White", year: 2018, model: "740I").save()
        new CarModel(maker: "Toyota", color: "Silver", year: 2018, model: "Sequoia").save()
        new CarModel(maker: "Acura", color: "Red", year: 2018, model: "MDX").save()


        Product.saveAll(
                new Product(name: 'Apple', price: 2.0),
                new Product(name: 'Orange', price: 3.0),
                new Product(name: 'Banana', price: 1.0),
                new Product(name: 'Cake', price: 4.0)
        )
    }
    def destroy = {
    }
}
