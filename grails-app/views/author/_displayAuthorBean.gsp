<tr>
    <td>
        <a href="/author/show/${author.id}">
            ${author.name}
        </a>
    </td>
    <td><g:render template="/book/displaybook" collection="${author.books}"/></td>
</tr>

