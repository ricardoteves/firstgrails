package firstgrails

class Author {
    String name
    String toString() {
        "${name}"
    }
    static hasMany = [books: Book]
    static constraints = {
    }
}
