package firstgrails

class Book {
    String title
    String isbn
    String toString() {
        "${title}"
    }

    static belongsTo = [author: Author]

    static constraints = {
        isbn(nullable:true,unique:true)
    }
}
