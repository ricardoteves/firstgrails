package firstgrails

class Vehicle {
    String plateNumber
    CarModel carModel
    Date start
    Date end
    String toString() {
        "${carModel.toString()}"
    }
    static belongsTo = [owner: Owner]
    static constraints = {
        plateNumber(nullable: true)
    }
}
