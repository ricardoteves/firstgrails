package firstgrails

class Owner {
    String name
    String address
    String city
    String state
    String zip
    String toString() {
        "${name} ${zip}"
    }
    static hasMany = [vehicles: Vehicle]
    static constraints = {
        name()
        address()
        city()
        state()
        zip()
    }
}
