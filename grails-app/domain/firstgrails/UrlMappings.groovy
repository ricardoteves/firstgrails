package firstgrails

class UrlMappings {
    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        '/products'(resources: 'product') {
            collection {
                '/search'(controller: 'product', action: 'search')
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
        "/book"(view: '/book/index') {
            "/author"(resources:"author")
        }
    }
}
